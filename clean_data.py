import pandas as pd
import numpy as np 

def drop_meta(df_in):
    """
    drop the first 2 rows & 12 columns of unnecessary metadata
    :param df_in: uncleaned dataframe
    :return: dataframe with filtered data
    """
    cleaned_data = data.iloc[2:, 11:]
    # drop the 2 columns with string values
    cleaned_data.drop(['Name_FMF', 'Name_FSF'], axis=1, inplace=True)
    # convert object to float
    cleaned_data = cleaned_data.astype('float')
    return cleaned_data

def simplify_cols(df_in, cols = ['M1','M2','M3','M4','S1','S2','S3','S4']):
    """
    move the M1-S4_XXX's into single columns (M1-S4)
    :param df_in: unsimplified data
    :param cols: newly created column names; default = M1-S4
    :return: data frame with simplied columns
    """
    simplified_data = pd.DataFrame()
    for i in new_cols:
        df_selected = cleaned_data[cleaned_data.columns[cleaned_data.columns.str.startswith(i)]] - 1
        simplified_data[i] = df_selected.sum(axis=1, skipna=True)
    return simplified_data
